package lesson6;

import java.util.Random;

public class StudentCalc {
    public static void main(String[] args) {

    /**
     * Создайте метод класса студент, который будет выводить всю
     * информацию о студенте.
     * Выведите информацию о всех отличниках (9-10 баллов за диплом) в
     * консоль.
     */

    String[] name = new String[]{"Саша", "Паша", "Даша", "Юля", "Наташа", "Вика", "Коля", "Дина", "Лиза", "Катя", "Вова","Петя","Глаша"}; //массив имен
    Student[] students = new Student[14]; // массив студентов
    for (int i = 0; i < students.length; i++) {
        Student student = new Student(name[getRandom(13)], getRandom(4), getRandom(10));
        students[i] = student;
    }

    /**
     * Выведите информацию о всех студентах из 3 группы в консоль.
     */

    System.out.println("Информация о студентах 3-ей группы:");
    for (int i = 0; i < students.length; i++) {
        students[i].getSudent3Group();
    }

    /**
     * Выведите информацию о всех отличниках (9-10 баллов за диплом)
     */
    System.out.println(" ");
    System.out.println("Вывод отличников: ");
    for (int i = 0; i < students.length; i++) {
        students[i].getOnlyGoodStudent();
    }

    System.out.println(" ");
    System.out.println("Вывод всех студентов:");
    for (int i = 0; i < students.length; i++) {
        System.out.println(students[i].toString());
    }
}

    private static int getRandom(int maxLimit) {
        Random random = new Random();
        return random.nextInt(maxLimit);
    }
}