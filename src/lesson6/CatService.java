package lesson6;

public class CatService {
    /**Создайте класс кота с полями:
    имя
            возраст
    насыщение кота (количество корма)

    Создайте метод “кормежка”. Входным параметром укажите количество корма. Возвращаемое значение:
            true - кот наелся
 false - кот не наелся*/

        public static void main(String[] args) {
            Cat cat1 = new Cat("Barsik", 4,123);
            System.out.println(cat1.toString());
            System.out.println(cat1.food());
        }
}
