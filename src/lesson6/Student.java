package lesson6;

public class Student {
    String name; //поле или перермення экземпяра
    int group;
    int grade;

    Student(String name, int group, int grade) {
        this.name = name;
        this.group = group;
        this.grade = grade;
    }

    public int getGrade() {
        return grade;
    }

    public int getGroup() {
        return group;
    }
    //отличники
    public void getOnlyGoodStudent(){
        if(grade>=9){
            System.out.println(toString());
        }
    }

    // студенты 3-ей группы
    public void getSudent3Group(){
        if(group == 3){
            System.out.println(toString());
        }
    }

    @Override
    public String toString () {
        return "Имя: " + name + " Группа: " + group + " Оценка: " + grade;
    }
}