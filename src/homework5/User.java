package homework5;
//Создайте класс User с полями:
//имя
//логин
//
//В цикле создайте массив с пользователями(User).

public class User {
    public String name; // поля
    public String login;


    public void setLogin(String login) {
        this.login = login;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Имя " + name + " Логин " + login;
    }
}
