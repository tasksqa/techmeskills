package homework5;

import java.util.Random;

public class UserCalculation {
    public static void main(String[] args) {
        String[] name = new String[]{"Оля","Катя", "Петя", "Паша", "Лиза", "Алиса", "Коля", "Саша", "Даша","Таня"};
        String [] login = new String[]{"Star234", "Cat3", "P2424", "122QA","Zrew", "Sky","Dog132", "CatX","Dwde23", "Qze"};

        User[] users = new User[10];

        for(int i = 0; i<users.length; i++){
            User user = new User();
            user.setLogin(login[getRandom(10)]);
            user.setName(name[i]);
            users[i] = user;
        }
        for (int i = 0; i < users.length; i++) {
            System.out.println(users[i].toString());
        }

    }
    private static int getRandom(int maxLimit){
        Random random = new Random();
        return random.nextInt(maxLimit);
    }
}
