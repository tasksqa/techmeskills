package homework3;

import java.util.Random;
import java.util.Scanner;
public class HomeWork3 {
    public static void main(String[] args) {
        //Task1();
        Task2();
        //Task3();

    }

    /**
     * 1) Сделать два метода
     * а) метод sortMax в котором сортировать элементы одномерного массива по возрастанию
     * б) метод sortMin в котором сортировать элементы одномерного массива по убыванию
     * Для первого задания, используйте сортировку из классной работы.
     */
    private static void Task1() {
        System.out.println("Введите длину одномерного массива");
        Scanner sc = new Scanner(System.in);
        int lengthArray = sc.nextInt();
        int array[] = new int[lengthArray];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.round(Math.random() * 200 + 10)); //рандомный массив целых чисел в диапазоне (10,200)
        }

        System.out.println("Исходный массив: ");

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        sortMax(array);
        sortMin(array);

    }

    /**
     * сортировка массива в порядке убывания методом пузырька
     */
    private static void sortMax(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] < array[j + 1]) {
                    int temp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        System.out.println();// для переноса строки
        System.out.println("Массив в порядке убывания методом пузырька: ");
        for (int a : array) {
            System.out.print(a + " ");
        }
    }

    /**
     * сортировка массива в порядке возрастания методом пузырька
     */

    private static void sortMin(int[] arrMin) {
        for (int i = 0; i < arrMin.length; i++) {
            for (int j = 0; j < arrMin.length - 1; j++) {
                if (arrMin[j] > arrMin[j + 1]) {
                    int temp = arrMin[j + 1];
                    arrMin[j + 1] = arrMin[j];
                    arrMin[j] = temp;
                }
            }
        }
        System.out.println();
        System.out.println("Массив в порядке возрастания методом пузырька: ");
        for (int a : arrMin) {
            System.out.print(a + " ");
        }
    }

    /**
     * Сделать «глупую сортировку» одномерного массива
     * Пример по ссылке https://habr.com/ru/post/204600/
     * Просматриваем массив слева-направо и по пути сравниваем соседей.
     * Если мы встретим пару взаимно неотсортированных элементов, то меняем их местами и возвращаемся на круги своя,
     * то бишь в самое начало. Снова проходим-проверяем массив, если встретили снова «неправильную» пару соседних элементов,
     * то меняем
     */

    private static void Task2() {
        System.out.println("Введите длину одномерного массива");
        Scanner sc = new Scanner(System.in);
        int lengthSt = sc.nextInt();
        int arrSt[] = new int[lengthSt];

        for (int i = 0; i < arrSt.length; i++) {
            arrSt[i]=RandomInt();
        }

        System.out.println("Исходный массив: ");

        for (int i = 0; i < arrSt.length; i++) {
            System.out.print(arrSt[i] + " ");
        }
        sortMaxSt(arrSt);
        sortMinSt(arrSt);
    }


    private static int RandomInt(){
        Random rn = new Random();
        return rn.nextInt(200);
    }

    /**
     * Глупая сортировка массива в порядке убывания
     */
    private static void sortMaxSt(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] < array[j + 1]) {
                    int temp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temp;
                    j = 0;
                } else {
                    j++;
                }
            }
        }
        System.out.println();
        System.out.println("Глупая сортировка массива в порядке убывания:");
        for (int a : array) {
            System.out.print(a + " ");
        }
    }

    /**
     * Глупая сортировка массива в порядке возрастания
     */

    private static void sortMinSt(int[] arrMin) {
        for (int i = 0; i < arrMin.length; i++) {
            for (int j = 0; j < arrMin.length - 1; j++) {
                if (arrMin[j] > arrMin[j + 1]) {
                    int temp = arrMin[j + 1];
                    arrMin[j + 1] = arrMin[j];
                    arrMin[j] = temp;
                    j = 0;
                } else {
                    j++;
                }
            }
        }
        System.out.println();
        System.out.println("Глупая сортировка массива в порядке возрастания:");
        for (int a : arrMin) {
            System.out.print(a + " ");
        }
    }


    /**
     * Найти минимум в двумерном массиве
     */
    private static void Task3() {
        System.out.println("Введите число строк и число столбцов массива");
        Scanner sc = new Scanner(System.in);
        int Arr[][] = new int[sc.nextInt()][sc.nextInt()];

        for (int i = 0; i < Arr.length; i++) {
            for (int j = 0; j < Arr[i].length; j++) {
                System.out.println("Введите значение элемента для позиции " + i + " " + j);
                Arr[i][j] = sc.nextInt();
            }
        }

        // Вывод исходного массива
        System.out.print("Исходный массив");
        for (int i = 0; i < Arr.length; i++) {
            System.out.println();
            for (int j = 0; j < Arr[i].length; j++) {
                System.out.print(Arr[i][j] + " ");
            }
        }
        MaxValue(Arr);
        MinValue(Arr);
    }

    private static void MaxValue(int myArr[][]) {
        System.out.println();
        int maxArr = myArr[0][0];
        for (int i = 0; i < myArr.length; i++) { //по строкам
            int maxInStr = myArr[i][0]; //максимальный элемент первый в строке
            for (int j = 1; j < myArr[i].length; j++) { // по столбцам, столбец идет с 1
                if (myArr[i][j] > maxInStr)
                    maxInStr = myArr[i][j];
            }
            System.out.println("Максимум в строке: " + maxInStr);

            if (maxInStr > maxArr) {
                maxArr = maxInStr;

            }
        }
        System.out.println();
        System.out.println("Максимальный эллемент массива: " + maxArr);
        System.out.println();

    }

    private static void MinValue(int myArr[][]){

        int minArr = myArr[0][0];

        for (int i=0; i< myArr.length; i++) { //по строкам
            int minInStr = myArr[i][0]; //минимальный элемент первый в строке
            for (int j = 1; j < myArr[i].length; j++) { // по столбцам, столбец идет с 1
                if (myArr[i][j] < minInStr)
                    minInStr = myArr[i][j];
            }
            System.out.println("Минимум в строке: " + minInStr);

            if (minInStr < minArr) {
                minArr = minInStr;
            }
        }
        System.out.println();
        System.out.println( "Минимальный эллемент массива: " + minArr );
    }
}



