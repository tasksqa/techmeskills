package homework10.task3;

import java.util.Scanner;

/**Ввести n строк с консоли. Вывести на консоль те строки, длина которых больше средней,
 * а также длину.
 для вывода результат используйте StringBuilder*/

public class StringApp {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите количество строк :");
        int n = sc.nextInt();

        String[] arrayStr = new String[n]; //массив строк
        int[] lengthStr = new int[n]; // массив длин строк;когда вводим строки, появляется Объект Строка со свойством Длина
        System.out.println("Введите " + n + " строк ");
        int sum = 0;
        for (int i = 0; i < arrayStr.length; i++) {
            arrayStr[i] = sc.next(); //заполняем массив строк
            lengthStr[i] = arrayStr[i].length(); // заполняем массив длин строк
            sum += arrayStr[i].length(); //сумма длин строк
        }
        int averageValue = sum / arrayStr.length;
        System.out.println("Среднее значение длины строк " + averageValue);

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < n; i++) {
            if (arrayStr[i].length() > averageValue) {
                builder.append("Строка: ").append(arrayStr[i]).append(" c длиной: ").append(lengthStr[i]).append("\n");
            }
        }
        System.out.println(builder);

    }
}