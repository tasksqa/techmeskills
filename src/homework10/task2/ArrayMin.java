package homework10.task2;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**Вводить значения из консоли и выводить минимум
 Используя одну из реализаций List(LinkedList и ArrayList)*/
public class ArrayMin {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите длину массива");
        int n = sc.nextInt();
        List<Integer> numbers = new LinkedList<>();
        System.out.println("Введите данные для массива");
        for ( int i = 0;  i<n; i++){
            numbers.add(sc.nextInt());
        }

        int minValue = numbers.get(0);
        for(int i = 0;  i<numbers.size(); i++){
            Integer number = numbers.get(i);
            if(number<minValue)
                number=minValue;
            }
        System.out.println("Минимальное значение " + minValue);
        }
    }



