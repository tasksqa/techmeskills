package homework10.task1;

public class Car {
    //параметры
    String model;
    int speed;
    int number;

    private int year =2010;

    // конструктор с параметрами
    Car(String inputModel,int inputSpeed){
        model = inputModel;
        speed = inputSpeed;
    }

    // конструктор без параметров
    Car(){
    }
    // метод для задания параметроа
    public void setNumber(int number){
        this.number= number;
    }
    //метод для получения параметров
 public int getYear(){
    return year;
    }

    public void displayInfo(String text) {
        System.out.println( text+ " год "+ year +" cкорость " +speed +" модель " + model);

    }
}
