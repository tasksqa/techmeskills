package homework10.task1;

public class CarApplication {
    public static void main(String[] args) {
        String text = "Тестовое задание";
        Car car1 = new Car("BMW",150); // создаем объект класса Car через конструктор с параметрами
        Car car2 = new Car();//через конструктор без параметров
        car2.setNumber(3); //задаем значение через метод setNumber
        car2.model = "Lada"; //через свойство

        //способ 1 - возвращаем значение year из Car через запись в переменную
        int car2year = car2.getYear();
        System.out.println(car2year);

        //способ2 - смотрим значение сразу чеоез геттер
        System.out.println(car1.getYear());
        //Выводить данные с текстом text
        car2.displayInfo(text);



    }
}
