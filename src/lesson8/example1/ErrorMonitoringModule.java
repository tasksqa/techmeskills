package lesson8.example1;

public class ErrorMonitoringModule implements MonitoringSystem {

    @Override
    public void startMonitoring() {
        System.out.println("Monitoring of errors has been started");

    }
}
