package lesson8.example1;

public class GeneralIndicatorMonitoringModule implements MonitoringSystem {

    @Override
    public void startMonitoring() {
        System.out.println("Monitoring of general systems has been started ");
    }
}
