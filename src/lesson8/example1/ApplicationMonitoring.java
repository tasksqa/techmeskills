package lesson8.example1;

public class ApplicationMonitoring { //базовый класс

    private static int countError = 3;
    public static void main(String[] args) {
        MonitoringSystem generalIndicator = new GeneralIndicatorMonitoringModule(); //интерфейс объект не создаст но модно приводрить к типу
        ErrorMonitoringModule errorMonitoring = new ErrorMonitoringModule();

        generalIndicator.startMonitoring();
        errorMonitoring.startMonitoring();

        // реализация анонимного класса
        MonitoringSystem monitoringSystem = new MonitoringSystem() {
            @Override
            public void startMonitoring() {
                System.out.println("Мониторинг стартовал");
            }
            public void printCountError(){
                System.out.println("Count errors" + countError );
            }
        };
    }
}
