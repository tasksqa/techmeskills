package lesson8.example3;

import java.util.Scanner;

public class DayWeeksApplication {
    public static void main(String[] args) {
        System.out.println("Введите номер дня недели:");
        Scanner sc = new Scanner(System.in);
        int paramFromScanner = sc.nextInt();

        System.out.println(DayWeeks.getValueOfDayWeekByNumber(paramFromScanner));

    }
}
