package lesson8.example3;

public enum DayWeeks {
    MON(1, "Понедельник"), //объекты
    TUE(2, "Вторник"),
    WED(3, "Среда"),
    THU(4, "Четверг"),
    FRI(5, "Пятница"),
    SAT(6, "Суббота"),
    SUN(7, "Воскресенье");

    //свойства(параметры)
    int numberOfDay;
    String nameOfDay;

    // конструктор
    DayWeeks(int numberOfDay, String inputName) {
        this.numberOfDay = numberOfDay;
        nameOfDay = inputName;
    }

    // Статичный метод к которому образатся только серез класс те dayWeeks.
    public static String getValueOfDayWeekByNumber(int paramFromScanner) {
        DayWeeks[] dayWeeks = DayWeeks.values(); // у enum есть метод values, который возвращает объектоы в массив
        for (DayWeeks dayWeek : dayWeeks) {
            if (dayWeek.numberOfDay == paramFromScanner) {
                return dayWeek.nameOfDay;
            }
        }
        return null;
    }
}
