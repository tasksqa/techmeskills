package lesson8.example2;

public class SeasonApplication {
    public static void main(String[] args) {
        //Пример 1 обращение к свойству объекта
        Season objSeason = Season.SUMMER;//objSeason - объект, который вернул Season.SUMMER
        // System.out.println(Season.SUMMER);

        String result = objSeason.getSeason(); //через объект потому что метод не статический
        System.out.println(result);

        //обращение к свойству возвращаемого объекта
        System.out.println(Season.AUTHUMN.getSeason());

    }
}
