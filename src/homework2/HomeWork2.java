package homework2;

import java.util.Random;
import java.util.Scanner;

public class HomeWork2 {
    public static void main(String[] args) {
        Task1();
        Task2();
        Task3Vers1();
        Task3();
        Task4();
    }

    /**
     * Даны 3 целых числа. Найти количество положительных чисел в исходном наборе.
     */
    private static void Task1() {

        int[] mas = new int[]{3, -4, 8};

        System.out.print("Исходный массив: ");
        for (int i = 0; i < mas.length; i++) {
            System.out.print(mas[i] + " ");
        }

        int pos = 0;

        for (int i = 0; i < mas.length; i++) {
            if (mas[i] > 0) {
                pos++;
            }
        }
        System.out.println();
        System.out.println("Количество положительных чисел " + pos);

        /**через тернарный оператор*/

        /*n= a>0?n+1:n;
        n= b>0?n+1:n;
        n= c>0?n+1:n;
        System.out.println("Количество положительных чисел в исходном наборе= " +n);*/
    }

    /**
     * Три целых числа вводятся из консоли. Найти количество положительных и отрицательных чисел
     * в исходном наборе.
     */
    static void Task2() {
        Scanner sc = new Scanner(System.in);
        int[] mas = new int[3]; // массив из 3-x эллементов типа int

        for (int i = 0; i < mas.length; i++) {
            System.out.println("Введите значение элемента для позиции " + i + " ");
            mas[i] = sc.nextInt();
        }

        System.out.print("Исходный массив: ");
        for (int i = 0; i < mas.length; i++) {
            System.out.print(mas[i] + " ");
        }

        int pos = 0;
        int neg = 0;

        for (int i = 0; i < mas.length; i++) {
            if (mas[i] > 0) {
                pos++;
            }else if(mas[i]<0) { //чтобы учесть 0
                neg++;
            }
        }
        /** цикл через for each
         * for(int ellement:mas){
         * element = sc.nextInt()
         * if(element>0){
         * pos++
         * } else if( ....
         */
        System.out.println();
        System.out.println("Количество положительных чисел " + pos);
        System.out.println("Количество отрицательных чисел " + neg);
    }

    /**
     * Найти минимум в массиве
     */
    public static void Task3Vers1() {
        int[] mas = new int[10]; // массив из 10 эллементов типа int

        for (int i = 0; i < mas.length; i++) {
            mas[i] = RandomInt();// заполняет массив рандомными числами в границах 100
            System.out.println("mas [" + i + "]=" + mas[i] + ";");// выводим массив заполненный рандомными числами
        }
        min(mas); // минимум
    }

    private static int RandomInt() {
        Random rn = new Random();//создаем генератор случайных чисел
        return rn.nextInt(100);// снова передает управление объекту, который вызвал данный метод
    }

    /** поиск минимума */
    private static void min(int[] mas) {
        int minValue = mas[0];

        for (int i = 1; i < mas.length; i++) {
            if (mas[i] < minValue)
                minValue = mas[i];
        }
        System.out.println("Минимум: " + minValue);
    }

    /**
     * Второй вариант заполнения массива
     */
    private static void Task3() {

        System.out.println("Введите длину массива");
        Scanner sc = new Scanner(System.in);
        int lengthArr = sc.nextInt();

        int arr[] = new int[lengthArr]; //массив из lengthArr эддементов типа int
        //int arr[] = new int[10]; // массив из 10-ти эллементов

        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) Math.round(Math.random() * 100); // Math.random() возвращает случайное число с плавающей запятой  [0,1)
            //Умножая это значение на 100, получаем случайное число в диапазоне [0,100)
            //Math.round() — округляем до целого
            System.out.println("arr [" + i + "]=" + arr[i] + ";");
        }

        int minValue = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < minValue)
                minValue = arr[i];
        }
        System.out.println("Минимум массива :" + minValue);
    }

    /**
     * (Дополнительно) В переменную записываете количество программистов.
     * В зависимости от количества программистов необходимо вывести правильно окончание.
     * Например: • 2 программиста • 1 программиста • 10 программистов • и т.д
     */
    private static void Task4() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число программистов");
        int x = sc.nextInt();
        int p = x % 10;
        int f = x % 100;
        if (f >= 11 && f <= 19) {
            switch (f) {
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                    System.out.println(+x + " программистов");
                    break;
            }
        } else {
            switch (p) {
                case 0:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    System.out.println(+x + " программистов");
                    break;
                case 1:
                    System.out.println(+x + " программист");
                    break;
                case 2:
                case 4:
                case 3:
                    System.out.println(+x + " программиста");
                    break;
                default:
                    System.out.println("Вы ввели недопустимое значение");
            }
        }
    }
}





