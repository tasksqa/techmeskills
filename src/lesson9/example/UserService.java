package lesson9.example;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserService {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите имя");
        User user1 = new User();
        String inputName = sc.next();
        System.out.println("Считал "+ inputName );
        try {
            user1.name(inputName);
        } catch (InputMismatchException e){
            System.out.println("Введите корректное значение имени. иначе программа завершится");
            user1.name(sc.next());
        }
    }
}
