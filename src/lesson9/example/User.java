package lesson9.example;

import java.util.InputMismatchException;

public class User {
    String name;

    public String name(String name)  throws InputMismatchException{ // означает что исключение перебрасывается дальше

        if (name.contains("1") ||
                name.contains("2") ||
                name.contains("3") ||
                name.contains("4")) {
            System.out.println("Error");
            throw new InputMismatchException();

        }
        this.name = name;
        return this.name;
    }
}
