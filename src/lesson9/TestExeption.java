package lesson9;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TestExeption {
    public static void main(String[] args) {
        System.out.println("Введите значения массива");
        try { //ловим исключение
            Scanner sc = new Scanner(System.in);
            int[] array = new int[3];
            for (int i = 0; i < array.length; i++) {
                array[i] = sc.nextInt();
                System.out.println(array[i]);
            }

        } catch (ArrayIndexOutOfBoundsException x) {
            System.out.println("Проблема с индексом");

        } catch (InputMismatchException t) { // выводим ошибку
            System.out.println("Вы ввели неверные данные " + t.fillInStackTrace());
        } finally {//пишем сообщение, что блок завершен
            System.out.println("Расчет окончен");
        }

    }
}
