package homework8.task2;

public class Rectangle extends Shape {
    protected double wight;
    protected double height;

   public double wight (double inputWight) {
       if(inputWight<=0){
           System.out.println("Значение ширины не может быть меньше или равно 0");
           throw new IllegalArgumentException("Incorrect value");
       }
       wight = inputWight;
       return wight;
   }

   public double height(double  inputHeight){
       if(inputHeight<=0){
           System.out.println("Значение длины не может быть меньше или равно 0");
           throw new IllegalArgumentException("Incorrect value");
       }
        height=inputHeight;
        return height;
   }

    @Override
    public double getArea() {
        area = wight * height;
        return area;
    }

    @Override
    public double getPerimetr() {
        perimetr = 2*(wight+height);
        return perimetr;
    }
}
