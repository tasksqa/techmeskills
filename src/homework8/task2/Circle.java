package homework8.task2;

public class Circle extends Shape {
    protected double radius;

   public double radius (double inputRadius) {
       if (inputRadius <= 0) {
           System.out.println("Значение радиуса не может быть меньше или равно 0");
           throw new IllegalArgumentException("Incorrect value");
       }
           radius = inputRadius;
           return radius;
       }


    @Override
    public double getArea() {
        area = 3.14*(radius*radius);
        return area;
    }

    @Override
    public double getPerimetr() {
        perimetr = 2*3.14*radius;
        return perimetr;
    }
}