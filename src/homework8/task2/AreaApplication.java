package homework8.task2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class AreaApplication {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Circle circle = new Circle();
        Rectangle rectangle = new Rectangle();



        try {
            System.out.println("Введите радиус окружности");
            double inputRadius =  sc.nextDouble();
            circle.radius(inputRadius);

            System.out.println("Площадь окружности: " + circle.getArea());
            System.out.println("Периметр окружности: " + circle.getPerimetr());


        }  catch (IllegalArgumentException x){
            System.out.println("Введите корректное значение радиуса или программа завершится");
            circle.radius(sc.nextDouble());
            System.out.println("Площадь окружности: " + circle.getArea());
            System.out.println("Периметр окружности: " + circle.getPerimetr());
        }
        catch (InputMismatchException e) {
            System.out.println("Вы ввели некорректные данные. Программа завершена с ошибкой " + e.fillInStackTrace());
            }


        try {
            System.out.println("Введите ширину и длину прямоугольника");
            double inputWight = sc.nextDouble();
            double inputHeight = sc.nextDouble();
            rectangle.wight(inputWight);
            rectangle.height(inputHeight);
            System.out.println("Площадь прямоугольника: " + rectangle.getArea());
            System.out.println("Периметр прямоугольника: " + rectangle.getPerimetr());

        } catch (IllegalArgumentException x){
            System.out.println("Введите корректное значение ширины и длины или программа завершится");
            rectangle.wight(sc.nextDouble());
            rectangle.height(sc.nextDouble());
            System.out.println("Площадь прямоугольника: " + rectangle.getArea());
            System.out.println("Периметр прямоугольника: " + rectangle.getPerimetr());
        }
        catch (InputMismatchException e) {
            System.out.println("Вы ввели некорректные данные. Программа завершена с ошибкой " + e.fillInStackTrace());

        }
    }
}
