package homework8.task2;

public abstract class Shape {

    protected double area;
    protected double perimetr;

    public abstract double getArea();

    public abstract double getPerimetr();
}

