package homework8.task1;

// Создайте ENUM со всеми месяцами года. И выведите названия месяца по номеру месяца в году.
// C вложенным свойством.

public enum Months {
    JANUARY(1, "Январь"),  // статически доступные экземпляры класса enum
    FEBRUARY(2, "Февраль"),
    MARCH(3, "Март"),
    APRIL(4, "Апрель"),
    MAY(5, "Май"),
    JUNE(6, "Июнь"),
    JULY(7, "Июль"),
    AUGUST(8, "Август"),
    SEPTEMBER(9, "Сентябрь"),
    OCTOBER(10, "Октябрь"),
    NOVEMBER(11, "Ноябрь"),
    DECEMBER(12, "Декабрь");
    //поля

    private int numberOfMonth;
    private String nameOfMonth;

    //конструктор
    Months(int inputMonthNumber, String inputMonthName) {
        numberOfMonth = inputMonthNumber;
        nameOfMonth = inputMonthName;
    }

    public static String getValueMonthNumber(int monthNumberConsole) { //входной параметр для метода
        Months[] numbersMonths = Months.values(); //values возвращает объекты в массив
        for (Months month : numbersMonths) {
            if (month.numberOfMonth == monthNumberConsole) {
                return month.nameOfMonth; //возвращает название месяца
            }

        }
        return "Месяца с таким номером не существует";
    }
}

