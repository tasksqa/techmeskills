package homework9;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserService {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        User user = new User();

        try {
            System.out.println("Введите имя пользователя:");
            String inputName = sc.next();
            System.out.println("Считано: "+ inputName);
            user.name(inputName);

        } catch (InputMismatchException e){
            System.out.println("Введите корректное значение имени или программа завершится");
            user.name(sc.next());

        }

        try {
            System.out.println("Введите фамилию пользователя:");
            String inputSurname = sc.next();
            System.out.println("Считано: "+ inputSurname);
            user.surname(inputSurname);

        } catch (InputMismatchException e){
            System.out.println("Введите корректное значение фамилии или программа завершится");
            user.surname(sc.next());
        }
    }
}
