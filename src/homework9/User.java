package homework9;

import java.util.InputMismatchException;

public class User {
    String name;
    String surname;

    public String name(String inputName) throws InputMismatchException { //означает, что метод выбрасывает исключение без обработки:объявления исключения
        // обязательно указывать throws для всех исключений, кроме тех, которые относятся к классам Error и RuntimeException

        if (inputName.contains("1") ||
                inputName.contains("2") ||
                inputName.contains("3") ||
                inputName.contains("4") ||
                inputName.contains("5") ||
                inputName.contains("6") ||
                inputName.contains("7") ||
                inputName.contains("8") ||
                inputName.contains("9"))
        {
            System.out.println("Вы ввели неверные данные");
            throw new InputMismatchException("Имя не может содержать цифры"); //при вызове метода возвращает исключение;явное создания исключения
        }
        name= inputName;
        return name;
    }

   public String surname(String inputSurname) throws InputMismatchException {

        if (inputSurname.contains("1") ||
                inputSurname.contains("2") ||
                inputSurname.contains("3") ||
                inputSurname.contains("4") ||
                inputSurname.contains("5") ||
                inputSurname.contains("6") ||
                inputSurname.contains("7") ||
                inputSurname.contains("8") ||
                inputSurname.contains("9"))
        {
            System.out.println("Вы ввели неверные данные");
            throw new InputMismatchException("Фамилия не может содержать цифры");
        }
         surname = inputSurname;
         return surname;
    }
}


