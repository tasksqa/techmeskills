package lesson12.taskExeption;

import java.util.Scanner;

public class DivisionNull {
   /** Деление на ноль. И обработать исключение*/
   public static void main(String[] args) {
      try {
         Scanner sc = new Scanner(System.in);
         System.out.println("Введите делимое");
         int value =sc.nextInt();
         System.out.println("Введите делитель");
         int value2 = sc.nextInt();
         int result = value/value2;
         System.out.println(result);

      }  catch (ArithmeticException e){
         try {
            System.out.println("Делитель не может быть равен 0.Введите делимое и делитель повторно");
            Scanner sc1 = new Scanner(System.in);//нужно переопределять потому что если сканер словил исключение то потянeт его в catch
            System.out.println(sc1.nextInt()/sc1.nextInt());

         } catch (ArithmeticException x){
            System.out.println("На ноль делить нельзя. Программа завершена с ошибкой");
         }
      }
   }
}
