package lesson12.taskCollection;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;
/**Создать коллекцию HashSet с типом элементов String.
 Добавить в неё 10 строк: арбуз, банан, вишня, груша, дыня, ежевика, жень-шень, земляника, ирис, картофель.
 Вывести содержимое коллекции на экран, каждый элемент с новой строки.

 Добавить удаление элемента*/

public class SetOfFruitPro {
    public static void main(String[] args) {
        Set<String> fruits = new LinkedHashSet<>();// выводит по порядку
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 10; i++) { //10 строк
            fruits.add(sc.next());
        }
        printAllFruits(fruits);

        System.out.println("Если хотите удалить элемент введите ДА, для отмены введите НЕТ");
        String str = sc.next();
        while (!str.equals("НЕТ")) {
            if (str.equals("ДА")) {
                System.out.println("Введите элемент который хотите удалить");
                String rem = sc.next();
                fruits.remove(rem);
            }
            printAllFruits(fruits);
            System.out.println("Если хотите удалить элемент введите ДА, для отмены введите НЕТ");
            str = sc.next();
        }
    }

    //для вывода всех значений
    static void printAllFruits(Set<String> fruits) {
        for (String fruit : fruits) {
            System.out.println(fruit);
        }
    }
}


