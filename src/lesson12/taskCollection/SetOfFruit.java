package lesson12.taskCollection;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

/**Создать коллекцию HashSet с типом элементов String.
 Добавить в неё 10 строк: арбуз, банан, вишня, груша, дыня, ежевика, жень-шень, земляника, ирис, картофель.
 Вывести содержимое коллекции на экран, каждый элемент с новой строки.*/
public class SetOfFruit {
    public static void main(String[] args) {
        Set<String> fruits = new LinkedHashSet<>();// выводит по порядку
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 10; i++) { //10 строк
            fruits.add(sc.next());
        }
        //вывод через for each
        for (String fruit : fruits) {
            System.out.println("вывод через for each "+fruit);
        }

        //вывод через while
        Iterator value = fruits.iterator();//iterator - интерфейс
         while (value.hasNext()){ //смотрит, есть ли ссылка на следующий объект,если есть, то выводит
             System.out.println("ывод через while "+value.next());
         }
    }
}
