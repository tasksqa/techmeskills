package lesson12.taskCollection;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**Создать коллекцию HashMap<String, String>, занести туда 10 пар строк: арбуз – ягода, банан – трава, вишня – ягода,
 * груша – фрукт, дыня – овощ, ежевика – куст, жень-шень – корень, земляника – ягода, ирис – цветок, картофель – клубень.
 Вывести содержимое коллекции на экран, каждый элемент с новой строки*/
public class MapOfFruit {
    public static void main(String[] args) {
        Map<String, String> fruits = new HashMap<>();

        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < 3; i++) {
            System.out.println("Значение");
            String name = sc.next();
            System.out.println("Тип");
            String type = sc.next();
            fruits.put(name, type);
        }
        //вывод сразу
        for (Map.Entry elem : fruits.entrySet()) {
            System.out.println(elem);
        }
        //отдельный вывод через StringBuilder
        for (Map.Entry elem : fruits.entrySet()) {
            StringBuilder builder = new StringBuilder();
            System.out.println(builder.append(elem.getKey()).append("-").append(elem.getValue()));
        }
    }
}
