package lesson11;

import java.util.HashSet;
import java.util.Set;

public class SampleUseSet {
    public static void main(String[] args) {
         //Проверяет возможность добавить одинаковые строки

        Set<String> collect = new HashSet<>();
        collect.add("Petya");
        collect.add("Petya");
        collect.add("Petya2");

        for (String elem : collect){
            System.out.println(elem);
        }

    }
}
