package lesson11.example3;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SampleUseMapOfPersonByPhone {
    /**
     * Вывести имя абонента по номеру телефона
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Map<String, Person> persons = new HashMap<>();
        //Сложный ввод значений из консоли
        System.out.println("Введите имя");
        String name = sc.next();
        System.out.println("Введите телефон");
        String phone = sc.next();
        while (!name.equals("stop") && !phone.equals("stop")){
            Person person = new Person(name, phone); // Создаём объект Person
            persons.put(phone, person); // Кладем объект person в map по ключу в виде телефона
            System.out.println("Введите имя");
            name = sc.next();
            //Чтобы после команды stop не считывать телефон
            if (name.equals("stop")){
                break;
            }
            System.out.println("Введите телефон");
            phone = sc.next();
        }
        //Выводим все значения из map (в map persons есть ключи это номера, и значение это объект типа person)
        // вот значения мы получаем методом values()
        for (Person element: persons.values()) {
            System.out.println(element.toString());
        }

        System.out.println("Введите номер телефона, а вам программа выведет имя абонента");
        //Получаем person из map где хранятся все persons по ключу ввиде вводимого телефона
        Person person = persons.get(sc.next());
        //Достаём у person имя
        System.out.println(person.getName());

    }
}

