package lesson11;

import java.util.*;

/**Вывести среднее значение списка, останавливать программу если введено слово "stop" или отрицательное значение,
 * обработать исключение на ввод букв*/
public class AverageOfList {
    public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            List<Integer> list = new ArrayList<>();
            String inpStr = sc.next();
            while (!(inpStr.equals("stop") || inpStr.contains("-"))) {//!"не" пока не ввели стоп считываем значения
                try {
                    list.add(Integer.valueOf(inpStr));

                }catch (NumberFormatException  x ){
                    System.out.println("Вы ввели неверное значение.Попробуйте еще раз");
                }
               inpStr = sc.next();
            }
            int sumElement = 0;
            for (Integer element : list) {
                sumElement = sumElement + element;

            }
            double resultat = sumElement / list.size();
            System.out.println(resultat);

        }

    }

