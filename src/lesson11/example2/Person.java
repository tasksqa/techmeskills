package lesson11.example2;

import java.util.Objects;

public class Person {
    String name;
    String phone;
    Person (String name,String phone){
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }
}

