package lesson11.example2;

import lesson11.example2.Person;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SampleUseMapDifficultobject {
    public static void main(String[] args) {
        Map<String, Person> persons = new HashMap<>();
        Scanner sc = new Scanner(System.in);
        String value = sc.next();

        while (!value.equals("stop")) {
            System.out.println("Введите имя");
            String name = value;
            System.out.println("Введите телефон");
            String phone = sc.next();
            Person person = new Person(name, phone);
            value = sc.next();

        }
        for (Person element : persons.values()) {
            System.out.println(element.getName());
        }
    }
}
