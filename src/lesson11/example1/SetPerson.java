package lesson11.example1;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class SetPerson {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Set<Person> personSet = new HashSet<>(); //лучше использовать интерфейс
        String value = sc.next();

        while (!value.equals("stop")){
            Person person = new Person(value);
            personSet.add(person); // заносим в set объекты person
            value = sc.next();

        }
        for(Person element :personSet){
            System.out.println(element.getName());
        }
    }
}
