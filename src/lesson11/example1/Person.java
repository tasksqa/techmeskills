package lesson11.example1;

import java.util.Objects;
/** запись объекта типа Person в Set и вывод*/
public class Person {
    String name;
    String phone;
    Person (String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    //переопределение хеш кода. Если не переопределить, то будут записываться и объекты с одинаковыми значениями
    //если одинаковые имена у сложного объекта, то нужно переопределять
    //для переопределения класс Person -> Generate -> equals and hashCode
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return name.equals(person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
