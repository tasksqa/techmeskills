package lesson7.example2;

public class PrintableService {
    public static void main(String[] args) {
        Book book = new Book("War and peace", "Tolstoy", 1900);
        book.print();
    }
}
