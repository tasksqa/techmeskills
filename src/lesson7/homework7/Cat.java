package lesson7.homework7;

public class Cat extends Animal{
    public String eyesColor;

    public void setEyesColor(String eyesColor){
        this.eyesColor = eyesColor;
    }
    public void printVoice() {
        System.out.println("May");
    }

    public String getVoice() {
        return "May";
    }

    public void printInfo() {
        System.out.println(animalID + " " + name + " " + data + " " + eyesColor);
    }
}