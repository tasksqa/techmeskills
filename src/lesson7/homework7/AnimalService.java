package lesson7.homework7;

import java.util.Date;

public class AnimalService {

    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.setAnimalID(1);
        cat.setName("Barsik");
        Date data = new Date();
        cat.setData(data);
        cat.setEyesColor("Green");

        Dog dog = new Dog();
        dog.setWeight(56.23);
        dog.setAnimalID(2);
        dog.setName("Sharik");
        dog.setData(data);

        Tiger tiger = new Tiger();
        tiger.setCountEatenExployees(7);
        tiger.setEyesColor("Blue");
        tiger.setAnimalID(12);
        tiger.setName("Radga");
        tiger.setData(data);

        callPrint(cat);
        callPrint(dog);
        callPrint(tiger);

    }

    private static void callPrint (Animal animal){
        animal.printInfo();
        animal.printVoice();
        String voice = animal.getVoice();
        System.out.println(voice);
    }


}