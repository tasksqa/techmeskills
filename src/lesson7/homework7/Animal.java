package lesson7.homework7;
//Задание к 8 лекции: добавить метод голос который будет выводит сообщение животного,типа "Мяу" и тд
import java.util.Date;

public abstract class Animal {
    protected int animalID;
    protected String name;
    protected Date data;

    public void setAnimalID(int animalID) {
        this.animalID = animalID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public abstract void printVoice();
    public abstract String getVoice();

    public abstract void printInfo();

}
