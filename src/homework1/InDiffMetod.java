package homework1;

import java.util.Scanner;

public class InDiffMetod {
    public static void main(String[] args) {
        task1();
        task2();
        task3();
        task4();
        task6(3.14,5.67);
    }

    private static void task1 ()  {
        // Даны два числа, вывести меньшее из них
        System.out.println("Введите два целых числа");

        Scanner sc = new Scanner(System.in);
         int a = sc.nextInt();
         int b = sc.nextInt();

        if (a > b) {
            System.out.println("Меньшее число " +b);
        } else {
            System.out.println("Меньшее число " +a);

        }
    }

    private static void task2 () {
        //даны  два числа, вывести их сумму
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите целое число");
        int  c = sc.nextInt();

        System.out.println("Введите целое число");
        int d = sc.nextInt();

        System.out.println("Cумма чисел = "+(c+d));

    }

    private static void task3 () {
        //Даны три целочисленных числа и с плавающей точкой - вывести их произведение
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите целое число");
        int a1 = sc.nextInt();

        System.out.println("Введите целое число");
        int a2 = sc.nextInt();

        System.out.println("Введите число типа double");
        double f = sc.nextDouble();

        double pr = a1*a2*f;

        System.out.println("Произведение чисел= "+pr);
    }

    private static void task4 () {
        //Даны два числа вывести остаток от деления этих чисел
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите целое число");
        int n1 = sc.nextInt();

        System.out.println("Введите целое число");
        int n2 = sc.nextInt();

        System.out.println("Остаток от деления " + n1 % n2);


    }

    private static void task6 (double d1 , double d2 ) {
        //Даны два числа с плавающей точкой.Преобразовать их сумму к целочисленному значению

        int sum = (int)Math.round(d1) + (int)Math.round(d2); //функция округления
        System.out.println("Целочисленное значение суммы=" +sum);
    }
}
